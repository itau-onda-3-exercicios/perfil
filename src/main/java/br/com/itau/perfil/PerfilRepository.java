package br.com.itau.perfil;

import org.springframework.data.repository.CrudRepository;


public interface PerfilRepository extends CrudRepository<Perfil, String> {
	
}
