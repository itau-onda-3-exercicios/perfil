package br.com.itau.perfil;

import java.security.Principal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PerfilController {
	@Autowired
	PerfilRepository perfilRepository;
	
	@Autowired
	UsuarioClient usuarioClient;
	
	@GetMapping("/quem/xablau")
	public Perfil exibirXablau() {
		Perfil perfil = new Perfil();
		
		perfil.setNome("Xablau");
		perfil.setEmail("xablau@gmail.com");
		perfil.setSenha("xablau123");
		
		return perfil;
	}
	
	@GetMapping
	public ResponseEntity consultar(Principal principal) {
		Optional<Perfil> perfilOptional = perfilRepository.findById(principal.getName());
		
		if(perfilOptional.isPresent()) {
			return ResponseEntity.ok(perfilOptional.get());
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping
	public Perfil criar(@RequestBody Perfil perfil) {
		usuarioClient.inserir(perfil);
		return perfilRepository.save(perfil);
	}
}
